<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone')->nullable();
            $table->string('password');
            $table->string('avatar')->default('avatar.png');
            $table->string('cover')->default('default');
            $table->longText('about')->nullable();
            $table->date('birthday')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('is_admin')->nullable()->default(false);
            $table->boolean('is_mentor')->nullable()->default(false);
            $table->boolean('is_organization')->nullable()->default(false);
            $table->boolean('is_referrer')->nullable()->default(false);
            $table->timestamp('last_login')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->string('activation_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
