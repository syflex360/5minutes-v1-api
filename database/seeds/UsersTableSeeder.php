<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User([
            'name' => '5minutes',
            'email' => 'admin@5minutes.one',
            'password' => bcrypt('secret'),
            'phone' => '07088886806',
            'about' => '5minutes support',
            'is_admin' => true,
            'is_mentor' => true,
            'is_organization' => true,
            'is_referrer' => true
        ]);

        $user->save();

        $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
        Storage::put('avatars/'.$user->id.'/avatar.png', (string) $avatar);
    }
}
